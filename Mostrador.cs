﻿using System;
using System.Text;

namespace StringManipulator
{
    internal class Mostrador
    {
        public static void Mostrar(string operacion, string cadenaOriginal, object resultado)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"\nCadena original: {cadenaOriginal}");
            sb.AppendLine($"Operación {operacion}: {resultado}");
            Console.WriteLine(sb.ToString());
        }

        public static void MostrarComparacion(string cadenaOriginal, string cadenaAComparar, bool sonIguales)
        {
            Mostrar("3", cadenaOriginal, $"Comparar con: {cadenaAComparar}, Son iguales: {sonIguales}");
        }

        public static void MostrarBusquedaSubcadena(string cadenaOriginal, string subcadena, bool contieneSubcadena)
        {
            Mostrar("3", cadenaOriginal, $"Buscar subcadena: {subcadena}, Contiene subcadena: {contieneSubcadena}");
        }
    }
}