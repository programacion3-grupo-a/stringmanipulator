﻿namespace StringManipulator
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string cadenaInicial = "Hola mundo";
            StringManipulator manipulador = new StringManipulator(cadenaInicial);

            char[] arrayDeCaracteres = manipulador.DividirEnArray();
            Mostrador.Mostrar("1", cadenaInicial, $"Array de caracteres: {string.Join(", ", arrayDeCaracteres)}");

            string cadenaReconstruida = manipulador.UnirArrayEnString(arrayDeCaracteres);
            Mostrador.Mostrar("1", cadenaInicial, $"Cadena reconstruida: {cadenaReconstruida}");

            string subcadena = manipulador.ObtenerSubstring(0, 4);
            Mostrador.Mostrar("2", cadenaInicial, $"Substring (0, 4): {subcadena}");

            string cadenaAComparar = "Hola mundo";
            bool sonIguales = manipulador.CompararCadenas(cadenaAComparar);
            Mostrador.MostrarComparacion(cadenaInicial, cadenaAComparar, sonIguales);

            string subcadenaABuscar = "mundo";
            bool contieneSubcadena = manipulador.BuscarSubcadena(subcadenaABuscar);
            Mostrador.MostrarBusquedaSubcadena(cadenaInicial, subcadenaABuscar, contieneSubcadena);
        }
    }
}
