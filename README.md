# Descripción General

La aplicación `StringManipulator` ofrece funcionalidades para manipular cadenas a través de la clase `StringManipulator`. Este conjunto de operaciones incluye dividir y unir cadenas en arrays de caracteres, obtener subcadenas, comparar cadenas y buscar subcadenas.

## Clase `StringManipulator`

**Constructor:**

- `public StringManipulator(string inputCadena)`: Inicializa la clase con la cadena de entrada.

**Métodos:**

- `public char[] DividirEnArray()`: Divide la cadena en un array de caracteres.
- `public string UnirArrayEnString(char[] arreglo)`: Une un array de caracteres en una cadena.
- `public string ObtenerSubstring(int startIndex, int length)`: Obtiene una subcadena.
- `public bool CompararCadenas(string otraCadena)`: Compara con otra cadena.
- `public bool BuscarSubcadena(string subcadena)`: Busca una subcadena.

## Clase `Mostrador`

**Métodos:**

- `public static void Mostrar(string operacion, string cadenaOriginal, object resultado)`: Muestra resultados de operaciones.
- `public static void MostrarComparacion(string cadenaOriginal, string cadenaAComparar, bool sonIguales)`: Muestra comparaciones.
- `public static void MostrarBusquedaSubcadena(string cadenaOriginal, string subcadena, bool contieneSubcadena)`: Muestra búsquedas de subcadenas.

## Uso

1. Crea una instancia de `StringManipulator` con una cadena inicial.
1. Realiza operaciones de manipulación de cadenas utilizando los métodos de `StringManipulator`.
1. Muestra los resultados en la consola usando los métodos de `Mostrador` en el método `Main` de la clase `Program`.