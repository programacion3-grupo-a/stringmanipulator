﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringManipulator
{
    public class StringManipulator
    {
        private readonly string cadena;

        public StringManipulator(string inputCadena)
        {
            cadena = inputCadena;
        }

        public char[] DividirEnArray()
        {
            return cadena.ToCharArray();
        }

        public string UnirArrayEnString(char[] arreglo)
        {
            return new string(arreglo);
        }

        public string ObtenerSubstring(int startIndex, int length)
        {
            return cadena.Substring(startIndex, length);
        }

        public bool CompararCadenas(string otraCadena)
        {
            return cadena.Equals(otraCadena);
        }

        public bool BuscarSubcadena(string subcadena)
        {
            return cadena.Contains(subcadena);
        }
    }
}
